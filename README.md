HubShark Standard Edition (se)
==============================

A Symfony project created to provide a complete core starting point for most sites.

This will be a feature filled blogging system, that  should easily expand to a community site.

### Features

_These are the features already implemented at an early stage in develpment._

* Login/Logout
* Admin Area
* Static Pages
* Blog Posts
* Tags
* EU COOKIE LAW Conform
* Some Legal Example Pages
* Responsive Twig+Bootstrap
* German and English included
* Internationalized (i18n)

### How to use

I have not tested this on a live server yet. So I am not sure on the deployment yet. I have done one that was built a bit different than this version of the project. I will when ready for that.

You are welcome to experiment and let us know if you found a way. You can do this in the _issue_ area.


### Documentation

In the wiki link above we keep some documentation.

At the time the tutorial for buils this version (Standard Edition).

