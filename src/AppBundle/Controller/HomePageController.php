<?php
// src/AppBundle/Controller/HomePageController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
// Import new namespaces
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;


class HomePageController extends Controller {


    public function homepageAction() {      
        return $this->render('AppBundle:Blog:homepage.html.twig');       
    }


    public function aboutAction() {
        return $this->render('AppBundle:Pages:about.html.twig');
    }

    public function contactAction(Request $request)
    {
        $enquiry = new Contact();
        $form = $this->createForm(ContactType::class, $enquiry);

         $this->request = $request;
            if ($request->getMethod() == 'POST') {
            $form->bind($request);


    if ($form->isValid()) {


                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from HubShark')
                    ->setFrom('info@hubshark.com')
                    ->setTo($this->container->getParameter('app.emails.contact_email'))
                    ->setBody($this->renderView('AppBundle:Contact:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

            $this->get('session')->getFlashbag('hubshark-notice', 'Your contact enquiry was successfully sent. Thank you!');


        // Redirect - This is important to prevent users re-posting
        // the form if they refresh the page
        return $this->redirect($this->generateUrl('app_contact'));
    }
        }

        return $this->render('AppBundle:Contact:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    public function searchAction() {
        return $this->render('AppBundle:Search:search.html.twig');
    }

    public function sidebarAction() {
        return $this->render('AppBundle:Sidebar:sidebar.html.twig');
    }

}
